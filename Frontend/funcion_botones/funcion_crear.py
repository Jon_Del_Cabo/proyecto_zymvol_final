from tkinter import *
from tkinter import ttk
import os
import subprocess
from subprocess import Popen, PIPE, STDOUT
from Frontend.status_botones.desactivacion_botones import desactivar_botones_si_nada_seleccionado
from Frontend.actualizar import Update
from Frontend.verificacion.verificacion_crear import crear_error
from Frontend.trituradora import destruir

def ejecutar_a(self, ID, password, user, proyecto_a, tabla_proyectos, btn_1, btn_2, btn_3):
	self.ID_proyecto = ID.get()
	self.passwd = password.get()
	self.directorio_base = os.path.join("/home", user, "Proyectos")
	self.dir_desencriptado = os.path.join(self.directorio_base, self.ID_proyecto)
	self.dir_encriptado = os.path.join(self.directorio_base, "." + self.ID_proyecto + "_enc")
	subprocess.call(["mkdir", self.directorio_base])
	self.carpeta_1 = os.path.join(self.dir_desencriptado, "output")
	self.carpeta_2 = os.path.join(self.dir_desencriptado, "input")
	self.carpeta_3 = os.path.join(self.dir_desencriptado, "docs")
	self.carpeta_4 = os.path.join(self.dir_desencriptado, "scripts")

	self.zymvol = subprocess.Popen(["encfs", "-S", self.dir_encriptado, self.dir_desencriptado], stdout=PIPE, stdin=PIPE)
	self.input_string = "y\ny\n{passwd}\n{passwd}\n".format(passwd = password.get())
	self.prueba24 = self.zymvol.communicate(input= self.input_string.encode("utf-8"))[0]
	self.prueba24
	subprocess.Popen(["mkdir", self.carpeta_1, self.carpeta_2, self.carpeta_3, self.carpeta_4])
	destruir(proyecto_a)
	#correcion(ID_proyecto)
							
	crear_error(self, tabla_proyectos, self.ID_proyecto, self.dir_desencriptado)
	Update(tabla_proyectos)
	desactivar_botones_si_nada_seleccionado(tabla_proyectos, btn_1, btn_2, btn_3)	

def ejecutar_b(self, ID, password, user, proyecto_b, tabla_proyectos, btn_1, btn_2, btn_3):
	self.ID_proyecto = ID.get()
	self.passwd = password.get()
	self.directorio_base = os.path.join("/home", user, "Proyectos")
	subprocess.call(["mkdir", self.directorio_base])
	self.dir_desencriptado = os.path.join(self.directorio_base, self.ID_proyecto)
	self.dir_encriptado = os.path.join (self.directorio_base, "." + self.ID_proyecto + "_enc")
	self.carpeta_1 = os.path.join(self.dir_desencriptado, "JavaScript")
	self.carpeta_2 = os.path.join(self.dir_desencriptado, "HTML")
	self.carpeta_3 = os.path.join(self.dir_desencriptado, "CSS")
	self.carpeta_4 = os.path.join(self.dir_desencriptado, "scripts")

	self.zymvol = subprocess.Popen(["encfs", "-S", self.dir_encriptado, self.dir_desencriptado], stdout=PIPE, stdin=PIPE)
	self.input_string = "y\ny\n{passwd}\n{passwd}\n".format(passwd = password.get())
	self.prueba24 = self.zymvol.communicate(input= self.input_string.encode("utf-8"))[0]
	self.prueba24
	subprocess.Popen(["mkdir", self.carpeta_1, self.carpeta_2, self.carpeta_3, self.carpeta_4])
	destruir(proyecto_b)
	 
	crear_error(self, tabla_proyectos, self.ID_proyecto, self.dir_desencriptado)
	Update(tabla_proyectos)
	desactivar_botones_si_nada_seleccionado(tabla_proyectos, btn_1, btn_2, btn_3)
