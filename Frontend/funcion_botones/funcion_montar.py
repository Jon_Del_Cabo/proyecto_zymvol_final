from tkinter import *
import os
import subprocess
from subprocess import Popen, PIPE, STDOUT
from Backend.usuario import usuario
from Frontend.status_botones.desactivacion_botones import desactivar_botones_si_nada_seleccionado
from Frontend.actualizar import Update
from Frontend.verificacion.verificacion_montar import montar_error
from Frontend.trituradora import destruir

def mount (self, dir_desencriptado, dir_encriptado, password_enc, passwd, solicitud_passwd, tabla_proyectos, updated_item, btn_1, btn_2, btn_3):
	self.montar = subprocess.Popen(["encfs", "-S", dir_encriptado, dir_desencriptado], stdout=PIPE, stdin=PIPE)
	self.input_string = "{passwd}\n".format(passwd = password_enc.get())
	self.prueba24 = self.montar.communicate(input = self.input_string.encode("utf-8"))[0]
	self.prueba24
	destruir(solicitud_passwd)
	Update(tabla_proyectos)	
	montar_error(self, tabla_proyectos, updated_item, dir_desencriptado)
	desactivar_botones_si_nada_seleccionado(tabla_proyectos, btn_1, btn_2, btn_3)
