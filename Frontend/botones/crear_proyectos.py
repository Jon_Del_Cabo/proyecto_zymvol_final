from tkinter import *
from tkinter import ttk
import os
import subprocess
from subprocess import Popen, PIPE, STDOUT
from Backend.usuario import usuario
from Frontend.trituradora import destruir
from Frontend.funcion_botones.funcion_crear import ejecutar_a, ejecutar_b

class CrearProyecto:
	def __init__(self, tabla_proyectos, btn_1, btn_2, btn_3):
		self.user = usuario()
		self.new_window = Toplevel()
		self.new_window.geometry("500x400")
		self.new_window.configure(bg="#D1D6D6")
		self.new_window.minsize(width=500, height=400)
		self.new_window.maxsize(width=500, height=400)

		self.lbl10 = Label(self.new_window, text="¿Tipo de proyecto?", font=("Arial", 20), bg="#D1D6D6", fg="black")
		self.lbl10.place(x=20, y=20)
		
		self.opciones = ttk.Combobox(self.new_window, state="readonly")
		self.opciones["values"] = ["Proyecto A", "Proyecto B"]

		self.opciones.set("Seleccionar")
		self.opciones.place(x=20, y=80)

		self.lbl33 = Label(self.new_window, text="Según el tipo de proyecto que selecciones, A o B, aqui se mostrará su correspondiente definición y contenido.", bg="white", fg="black", font=("Arial", 10), wraplength=137, justify="left")
	    	
		self.lbl33.place(x=310, y=80, height=150)
		
		def proyecto_seleccionado (event):
			if (event.widget.get() == "Proyecto A"):
				self.lbl33.config(text="Al seleccionar el Proyecto A, va a obtener las siguientes carpetas: Docs, Scripts, Inputs, Outputs")
				def proyecto_a():
					self.ID = StringVar()
					self.password = StringVar()
											
					self.proyecto_a = Toplevel()
					self.proyecto_a.geometry("500x300")
					self.proyecto_a.configure(bg="#D1D6D6")
					self.proyecto_a.title("Proyecto A")
					self.proyecto_a.minsize(width=500, height=300)
					self.proyecto_a.maxsize(width=500, height=300)

					destruir(self.new_window)
					self.lbl = Label(self.proyecto_a, text="Configuración", bg="#D1D6D6", fg="black", font=("Arial", 18))
					self.lbl.place(x=75, y= 25)

					self.lbl2 = Label(self.proyecto_a, text="Nombre del proyecto:", bg="#D1D6D6", fg="black", font=("Arial", 12))
					self.lbl2.place(x=75, y= 80)

					self.lbl1 = Label(self.proyecto_a, text="Contraseña:", bg="#D1D6D6", fg="black", font=("Arial", 12))
					self.lbl1.place(x=75, y=150)
					
					self.nombre_proyecto = Entry (self.proyecto_a, fg="black", font="Arial", textvariable=self.ID)
					self.nombre_proyecto.place(x=75, y=105)
						
					self.contraseña_entry = Entry(self.proyecto_a, fg="black", font="Arial", textvariable=self.password)
					self.contraseña_entry.place(x=75, y= 175)

					self.btn = Button(self.proyecto_a, text="Realizar", bg="black", fg="white", command=lambda:ejecutar_a(self, self.ID, self.password, self.user, self.proyecto_a, tabla_proyectos, btn_1, btn_2, btn_3))
					self.btn.place(x=400, y= 250)
										
				self.btn32.config(command=proyecto_a)
				
			if (event.widget.get() == "Proyecto B"):
				self.lbl33.config(text="Al seleccionar el Proyecto B, va a obtener las siguientes carpetas: HTML, CSS, JavaScript")

				def proyecto_b():
					self.password = StringVar()	
					self.ID = StringVar()
					
					self.proyecto_b = Toplevel()
					self.proyecto_b.geometry("500x300")
					self.proyecto_b.configure(bg="#D1D6D6")
					self.proyecto_b.title("Proyecto B")
					self.proyecto_b.minsize(width=500, height=300)
					self.proyecto_b.maxsize(width=500, height=300)
					destruir(self.new_window)

					self.lbl = Label(self.proyecto_b, text="Configuración", bg="#D1D6D6", fg="black", font=("Arial", 18))
					self.lbl.place(x=75, y= 25)

					self.lbl2 = Label(self.proyecto_b, text="Nombre del proyecto:", bg="#D1D6D6", fg="black", font=("Arial", 12))
					self.lbl2.place(x=75, y= 80)

					self.lbl1 = Label(self.proyecto_b, text="Contraseña:", bg="#D1D6D6", fg="black", font=("Arial",12))
					self.lbl1.place(x=75, y=150)
					
					self.nombre_proyecto = Entry (self.proyecto_b, fg="black", font="Arial", textvariable=self.ID)
					self.nombre_proyecto.place(x=75, y=105)
						
					self.contraseña_entry2 = Entry(self.proyecto_b, fg="black", font="Arial", textvariable=self.password)
					self.contraseña_entry2.place(x=75, y= 175)

					self.btn = Button(self.proyecto_b, text="Realizar", bg="black", fg="white", command = lambda:ejecutar_b(self, self.ID, self.password, self.user, self.proyecto_b, tabla_proyectos, btn_1, btn_2, btn_3))
					self.btn.place(x=400, y= 250)
					
				self.btn32.config(command=proyecto_b)	
		
		self.opciones.bind("<<ComboboxSelected>>", proyecto_seleccionado)
		
		self.btn32 = Button(self.new_window, text="Siguiente", bg="black", fg="white", font=("Arial", 10))
		self.btn32.place(x=410, y=350)
