from tkinter import *
import os
import subprocess
from subprocess import Popen, PIPE, STDOUT
from Backend.usuario import usuario
from Frontend.status_botones.desactivacion_botones import desactivar_botones_si_nada_seleccionado
from Frontend.actualizar import Update
from Frontend.verificacion.verificacion_desmontar import desmontar_error
from Frontend.trituradora import destruir

class DesmontarProyecto:
	def __init__(self, tabla_proyectos, btn_1, btn_2, btn_3):
		def umount (updated_item, desmontar, dir_desencriptado):
			subprocess.call(["umount", dir_desencriptado])
			destruir(desmontar)
			desmontar_error(tabla_proyectos, updated_item, dir_desencriptado)
			desactivar_botones_si_nada_seleccionado(tabla_proyectos, btn_1, btn_2, btn_3)
			Update(tabla_proyectos)	
			
		self.user = usuario()
		self.directorio_base = os.path.join("/home", self.user, "Proyectos")

		self.desmontar = Toplevel()
		self.desmontar.geometry("500x210")
		self.desmontar.minsize(width=500, height=210)
		self.desmontar.maxsize(width=500, height=210)
		self.desmontar.configure(bg='#D1D6D6')
		self.desmontar.title("Desmontar proyecto")
		
		self.seleccionado = tabla_proyectos.selection()	
		self.item = [tabla_proyectos.item(self.seleccionado)["text"]]
		self.updated_item = str(self.item)[2:-2]
		self.dir_desencriptado = os.path.join(self.directorio_base, self.updated_item)
		
		self.texto_confirmacion = "¿Desea desmontar el proyecto " + self.updated_item + "?"
		self.confirmacion = Label(self.desmontar, bg="#D1D6D6", fg="black", font=("Arial",16), anchor="center", wraplength=400, text=self.texto_confirmacion)
		self.confirmacion.place(x=50, y=40, width=400)
		
		self.btn77 = Button(self.desmontar, text="Sí", bg="black", fg="white", command=lambda:umount(self.updated_item, self.desmontar, self.dir_desencriptado))
		self.btn77.place(x=280, y=120, width=75)
	
		self.btn78 = Button(self.desmontar, text="No", bg="black", fg="white", command=lambda:destruir(desmontar))
		self.btn78.place(x=140, y=120, width=75)
