from tkinter import *
import sys
import os
import subprocess
import getpass
from subprocess import Popen, PIPE, STDOUT
from Backend.usuario import usuario
from Frontend.status_botones.desactivacion_botones import desactivar_botones_si_nada_seleccionado
from Frontend.actualizar import Update
from Frontend.verificacion.verificacion_eliminar import eliminar_error
from Frontend.trituradora import destruir

class EliminarProyecto:
	def __init__(self, tabla_proyectos, btn_1, btn_2, btn_3):
		def eliminar_objeto(self, updated_item, dir_desencriptado, dir_encriptado, eliminar):
			subprocess.call(["fusermount", "-uz", dir_desencriptado])
			subprocess.call(["rm", "-rf", dir_desencriptado, dir_encriptado])
			destruir(eliminar)
			Update(tabla_proyectos)
			eliminar_error(self, tabla_proyectos, updated_item, dir_desencriptado)
			desactivar_botones_si_nada_seleccionado(tabla_proyectos, btn_1, btn_2, btn_3)	
		self.user = usuario()
		self.directorio_base = os.path.join("/home", self.user, "Proyectos")	
		
		self.eliminar = Toplevel()
		self.eliminar.geometry("500x210")
		self.eliminar.configure(bg='#D1D6D6')
		self.eliminar.title("Eliminar proyecto")

		self.seleccionado = tabla_proyectos.selection()	
		self.item = [tabla_proyectos.item(self.seleccionado)["text"]]
		self.updated_item = str(self.item)[2:-2]
		
		self.dir_desencriptado = os.path.join(self.directorio_base, self.updated_item)
		self.dir_encriptado = os.path.join (self.directorio_base, "." + self.updated_item + "_enc")

		self.texto_confirmacion = "¿Desea eliminar el proyecto " + self.updated_item + "?"
		self.confirmacion = Label(self.eliminar, bg="#D1D6D6", fg="black", font=("Arial",16), anchor="center", wraplength=400, text= self.texto_confirmacion)
		self.confirmacion.place(x=50, y=40, width=400)

		self.btn77 = Button(self.eliminar, text="Sí", bg="black", fg="white", command=lambda:eliminar_objeto(self, self.updated_item, self.dir_desencriptado, self.dir_encriptado, self.eliminar))
		self.btn77.place(x=280, y=120, width=75)

		self.btn78 = Button(self.eliminar, text="No", bg="black", fg="white", command=lambda:destruir(self.eliminar))
		self.btn78.place(x=140, y=120, width=75)
		
			
