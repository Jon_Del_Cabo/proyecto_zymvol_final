from tkinter import *
from tkinter import ttk
import os
import subprocess
from subprocess import PIPE
from Backend.usuario import usuario
from Frontend.trituradora import destruir

def desmontar_error(tabla_proyectos, updated_item, dir_desencriptado):
	status = 0    
	def mostrar_no_error_des(status): 
	    print(status)
	    if status == "Montado":
	        desencriptar_funciona = Toplevel()
	        desencriptar_funciona.geometry("500x150")
	        desencriptar_funciona.minsize(width=500, height=150)
	        desencriptar_funciona.maxsize(width=500, height=150)
	        desencriptar_funciona.configure(bg="#D1D6D6")
	        
	        lbl178 = Label(desencriptar_funciona, text="¡El proyecto se ha desmontado correctamente!", bg="#D1D6D6", fg="black", font=("Arial", 15))
	        lbl178.place(x=60, y=50)
	        
	        btn297 = Button(desencriptar_funciona, text="Aceptar", bg="black", fg="white", font=("Arial",10), command=lambda:destruir(desencriptar_funciona))
	        btn297.place(x=410, y=100)
	    else:
	        desencriptar_error = Toplevel()
	        desencriptar_error.geometry("500x150")
	        desencriptar_error.minsize(width=500, height=150)
	        desencriptar_error.maxsize(width=500, height=150)
	        desencriptar_error.configure(bg="#D1D6D6")
	        
	        lbl278 = Label(desencriptar_error, text="¡El proyecto no se ha podido desmontar!", bg="#D1D6D6", fg="black", font=("Arial", 15))
	        lbl278.place(x=60, y=50)
	        
	        btn297 = Button(desencriptar_error, text="Aceptar", bg="black", fg="white", font=("Arial",10), command=lambda:destruir(desencriptar_error))
	        btn297.place(x=410, y=100)

	def check_id_error_des(tabla_proyectos, updated_item, dir_desencriptado):
			user = usuario()
			directorio_base = os.path.join("/home", user, "Proyectos")
			carpetas = os.listdir(directorio_base)
			list_mounted_dir = subprocess.Popen(["df", "--output=target"], stdout=PIPE)
			lista_mounted_dir = str(list_mounted_dir.communicate())
			if str(updated_item[-1]) in str(lista_mounted_dir):
				status = "Montado"
				mostrar_no_error_des(status)
			else:
				status = "Desmontado"
				mostrar_no_error_des(status)

	check_id_error_des(tabla_proyectos, updated_item, dir_desencriptado)
	    
def si_desmontado(tabla_proyectos, btn_1, btn_2, btn_3):
	no_desmontar = Toplevel()
	no_desmontar.geometry("500x150")
	no_desmontar.minsize(width=500, height=150)
	no_desmontar.maxsize(width=500, height=150)
	no_desmontar.configure(bg='#D1D6D6')
	no_desmontar.title("Error montar proyecto")

	lbl888 = Label(no_desmontar, text="¡El proyecto ya esta desmontado!", bg="#D1D6D6", fg="black", font=("Arial", 15))
	lbl888.place(x=60, y=50)
	
	btn587 = Button(no_desmontar, text="Aceptar", bg="black", fg="white", font=("Arial",10), command=lambda:destruir(no_desmontar))
	btn587.place(x=410, y=100)
