from tkinter import *
from tkinter import ttk
import os
import subprocess
from subprocess import PIPE
from Backend.usuario import usuario
from Frontend.trituradora import destruir

def montar_error(tabla_proyectos, updated_item, dir_desencriptado):
	status = 0  
	def mostrar_no_error(status):  
		print(status)
		if status == "Montado":
			encriptar_funciona = Toplevel()
			encriptar_funciona.geometry("500x150")
			encriptar_funciona.minsize(width=500, height=150)
			encriptar_funciona.maxsize(width=500, height=150)
			encriptar_funciona.configure(bg="#D1D6D6")

			lbl178 = Label(encriptar_funciona, text="¡El proyecto se ha montado correctamente!", bg="#D1D6D6", fg="black", font=("Arial", 15))
			lbl178.place(x=60, y=50)
			
			btn297 = Button(encriptar_funciona, text="Aceptar", bg="black", fg="white", font=("Arial",10), command=lambda:destruir(encriptar_funciona))
			btn297.place(x=410, y=100)
            
		else:      
			encriptar_error = Toplevel()
			encriptar_error.geometry("500x150")
			encriptar_error.minsize(width=500, height=150)
			encriptar_error.maxsize(width=500, height=150)
			encriptar_error.configure(bg="#D1D6D6")
			
			lbl278 = Label(encriptar_error, text="¡La contraseña no es correcta!", bg="#D1D6D6", fg="black", font=("Arial", 15))
			lbl278.place(x=60, y=50)
			
			btn297 = Button(encriptar_error, text="Aceptar", bg="black", fg="white", font=("Arial",10), command=lambda:destruir(encriptar_error))
			btn297.place(x=410, y=100)
            
	def check_id_error(tabla_proyectos, updated_item, dir_desencriptado):
		user = usuario()
		directorio_base = os.path.join("/home", user, "Proyectos")
		carpetas = os.listdir(directorio_base)
		list_mounted_dir = subprocess.Popen(["df", "--output=target"], stdout=PIPE)
		lista_mounted_dir = str(list_mounted_dir.communicate())
		
		if str(updated_item[-1]) in str(lista_mounted_dir):
			status = "Montado"
			mostrar_no_error(status)
		else:
			status = "Desmontado"
			mostrar_no_error(status)
			
	check_id_error(tabla_proyectos, updated_item, dir_desencriptado)
	
def si_montado():
	        no_encriptar = Toplevel()
	        no_encriptar.geometry("500x150")
	        no_encriptar.minsize(width=500, height=150)
	        no_encriptar.maxsize(width=500, height=150)
	        no_encriptar.configure(bg='#D1D6D6')
	        no_encriptar.title("Error montar proyecto")

	        lbl888 = Label(no_encriptar, text="¡El proyecto ya esta montado!", bg="#D1D6D6", fg="black", font=("Arial", 15))
	        lbl888.place(x=60, y=50)

	        btn587 = Button(no_encriptar, text="Aceptar", bg="black", fg="white", font=("Arial",10), command=lambda:destruir(no_encriptar))
	        btn587.place(x=410, y=100)
