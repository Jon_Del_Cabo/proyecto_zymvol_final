from tkinter import *
from tkinter import ttk
import os
import subprocess
from subprocess import PIPE
from Backend.usuario import usuario
from Frontend.trituradora import destruir

def eliminar_error(self, tabla_proyectos, updated_item, dir_desencriptado):
    self.status = 0   
        
    def mostrar_no_error_elm(status):  
        if status != "Montado":
            self.eliminar_funciona = Toplevel()
            self.eliminar_funciona.geometry("500x150")
            self.eliminar_funciona.configure(bg="#D1D6D6")

            self.lbl178 = Label(self.eliminar_funciona, text="¡El proyecto se ha eliminado correctamente!", bg="#D1D6D6", fg="black", font=("Arial", 15))
            self.lbl178.place(x=60, y=50)
            
            self.btn297 = Button(self.eliminar_funciona, text="Aceptar", bg="black", fg="white", font=("Arial",10), command=lambda:destruir(self.eliminar_funciona))
            self.btn297.place(x=410, y=100)
            
        else:      
            self.eliminar_error = Toplevel()
            self.eliminar_error.geometry("500x150")
            self.eliminar_error.configure(bg="#D1D6D6")
            self.lbl278 = Label(self.eliminar_error, text="¡El proyecto no se ha podido eliminar!", bg="#D1D6D6", fg="black", font=("Arial", 15))
            self.lbl278.place(x=60, y=50)  
            
            self.btn297 = Button(self.eliminar_error, text="Aceptar", bg="black", fg="white", font=("Arial",10), command=lambda:destruir(self.eliminar_error))
            self.btn297.place(x=410, y=100)
            
    def check_id_error_elm(tabla_proyectos, updated_item, dir_desencriptado):
        self.user = usuario()
        self.directorio_base = os.path.join("/home", self.user, "Proyectos")
        self.carpetas = os.listdir(self.directorio_base)
        self.list_mounted_dir = subprocess.Popen(["df", "--output=target"], stdout=PIPE)
        self.lista_mounted_dir = str(self.list_mounted_dir.communicate())
        
        if str(self.updated_item[-1]) in str(self.lista_mounted_dir):
            self.status = "Montado"
            mostrar_no_error_elm(self.status)
            
        else:
            self.status = "Desmontado"
            mostrar_no_error_elm(self.status)

    check_id_error_elm(tabla_proyectos, updated_item, dir_desencriptado)
