from tkinter import *
from tkinter import ttk
import os
import subprocess
from subprocess import PIPE
from Backend.usuario import usuario
from Frontend.botones.desmontar_proyectos import DesmontarProyecto
from Frontend.botones.montar_proyectos import MontarProyecto
from Frontend.verificacion.verificacion_desmontar import si_desmontado
from Frontend.verificacion.verificacion_montar import si_montado
from Frontend.actualizar import Update
#from frontend.montar import no_montado, si_montado

class StatusYComprobaciones:
	def __init__(self, tp, btn_1, btn_2, btn_3):
		self.user = usuario()
		self.tabla_proyectos = tp
		self.btn_1 = btn_1
		self.btn_2 = btn_2
		self.btn_3 = btn_3
		self.directorio_base = os.path.join("/home", self.user, "Proyectos")
		self.list_mounted_dir = subprocess.Popen(["df", "--output=target"], stdout=PIPE)
		self.lista_mounted_dir = str(self.list_mounted_dir.communicate())			
		self.seleccionado = self.tabla_proyectos.selection()	
		self.item = [self.tabla_proyectos.item(self.seleccionado)["text"]]
		self.updated_item = str(self.item)[2:-2]

	def comprobar_estado_desmontar(self):
		if self.updated_item in str(self.lista_mounted_dir):
			DesmontarProyecto(self.tabla_proyectos, self.btn_1, self.btn_2, self.btn_3)
			Update(self.tabla_proyectos)	
		else:
			si_desmontado(self.tabla_proyectos, self.btn_1, self.btn_2, self.btn_3)
			Update(self.tabla_proyectos)	
				    
	def comprobar_estado_montar(self):
	    if self.updated_item in str(self.lista_mounted_dir):
		    si_montado()
		    Update(self.tabla_proyectos)
	    else:
		    MontarProyecto(self.tabla_proyectos, self.btn_1, self.btn_2, self.btn_3)
		    Update(self.tabla_proyectos)
