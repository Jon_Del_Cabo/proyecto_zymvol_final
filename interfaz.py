from tkinter import *
from tkinter import ttk
import os
import subprocess
from subprocess import PIPE
from Backend.usuario import usuario
from Frontend.status_botones.desactivacion_botones import desactivar_botones_si_nada_seleccionado
from Frontend.status_botones.activacion_botones import activar_botones
from Frontend.botones.crear_proyectos import CrearProyecto
from Frontend.botones.eliminar_proyectos import EliminarProyecto
from Frontend.botones.desmontar_proyectos import DesmontarProyecto
from Frontend.botones.montar_proyectos import MontarProyecto
from Frontend.actualizar import Update
from Backend.comprobar_estado import StatusYComprobaciones
#from Backend.comprobar_estado.py import comprobar_estado_montar

#		--- VENTANA PRINCIPAL ---

window = Tk()
window.title("Aplicación Zymvol")
window.configure(bg="#D1D6D6")
window.geometry("1280x720")
window.minsize(width=700, height=600)
window.maxsize(width=1920, height=1080)

class Interface:
	def __init__(self, main):
		
#		--- USUARIO ---		
		
		self.user = usuario()
		
#		--- DIRECTORIOS PRINCIPALES ---		
		
		self.directorio_proyectos = os.path.join("/home", self.user, "Proyectos")
		subprocess.call(["mkdir", self.directorio_proyectos])
		
# 		--- OBJETOS VENTANA PRINCIPAL ---	
		
		self.frame = Frame(main)
			
#		--- TITULO ---	
			
		self.lbl = Label(main, text="Nombre de la aplicación", font=("Arial", 25), bg="#2193D5", fg="black", anchor="w")
		self.lbl.place(relx=0.17, rely=0.1, relwidth=0.665)
		
#		--- TABLA DE DATOS ---
		
		self.estilo = ttk.Style()
		self.estilo.configure("mystyle.Treeview", font=("Arial", 12))
		self.estilo.configure("mystyle.Treeview.Heading", font=("Arial", 16))

		self.tree_frame = Frame(main)
		self.tree_frame.place(relx=0.17, rely=0.35, relwidth=0.665, relheight=0.4)

		self.tabla_proyectos = ttk.Treeview(self.tree_frame, selectmode=BROWSE, columns=("Ubicación", "Status","Backups"), style="mystyle.Treeview")
		self.tabla_proyectos.place(relwidth=1, relheight=1)
		self.tabla_proyectos.column("#0", width=130)
		self.tabla_proyectos.column("#2", width=115)
		self.tabla_proyectos.heading("#0", text="ID")
		self.tabla_proyectos.heading("#1", text="Ubicación")
		self.tabla_proyectos.heading("#2", text="Status") 
		self.tabla_proyectos.heading("#3", text="Último backup")
			
		self.verscrlbar = ttk.Scrollbar(self.tree_frame, orient="vertical")	
		self.verscrlbar.pack(side="right", fill="y")

		self.tabla_proyectos.configure(yscrollcommand=self.verscrlbar.set)	
		
#		--- ACTUALIZACIÓN TABLA ---

		Update(self.tabla_proyectos)	
		
#		--- BOTONES ---
		
		self.btn = Button (main, text="Crear proyecto", font=("Arial", 11), bg="black", fg="white", command=lambda:CrearProyecto(self.tabla_proyectos,self.btn_1, self.btn_2, self.btn_3))
		self.btn.place(relx=0.17, rely=0.18)

		self.btn_1 = Button (main, text="Montar proyecto", font=("Arial",11), bg="gray", fg="black", command=lambda:StatusYComprobaciones(self.tabla_proyectos, self.btn_1, self.btn_2, self.btn_3).comprobar_estado_montar())
		self.btn_1.place(relx=0.731, rely=0.18)
		
		self.btn_2 = Button (main, text="Desmontar proyecto", font=("Arial", 11), bg="gray", fg="black", command=lambda:StatusYComprobaciones(self.tabla_proyectos, self.btn_1, self.btn_2, self.btn_3).comprobar_estado_desmontar())
		self.btn_2.place(relx=0.711, rely=0.24)

		self.btn_3 = Button (main, text="Eliminar proyecto", font=("Arial",11), bg="gray", fg="black", command=lambda:EliminarProyecto(self.tabla_proyectos, self.btn_1, self.btn_2, self.btn_3))
		self.btn_3.place(relx=0.17, rely=0.24)
		
		desactivar_botones_si_nada_seleccionado(self.tabla_proyectos, self.btn_1, self.btn_2, self.btn_3)
		
		self.tabla_proyectos.bind("<<TreeviewSelect>>", lambda event:activar_botones(self, self.tabla_proyectos, self.btn_1, self.btn_2, self.btn_3))
usuario1 = Interface(window)
window.mainloop()
